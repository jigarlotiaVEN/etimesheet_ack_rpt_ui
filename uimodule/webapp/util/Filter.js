sap.ui.define([
  "./Filter"
], function () {
  "use strict";

  return {
    fnFilterforDate: function () {
     
      var date = new Date();
      var firstDay = new Date(date.getFullYear(), date.getMonth(), 1);
      var lastDay = new Date(date.getFullYear(), date.getMonth() + 1, 0);
      
      var oDateFormat = sap.ui.core.format.DateFormat.getDateInstance({
          pattern: "yyyy-MM-dd"
        });

      var firstDayFormatted = oDateFormat.format(firstDay);  
      var lastDayFormatted = oDateFormat.format(lastDay);  

      return firstDayFormatted, lastDayFormatted;

    }
  }

});