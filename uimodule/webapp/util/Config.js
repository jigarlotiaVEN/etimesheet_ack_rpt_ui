sap.ui.define([
  "./Config"
], function () {
  "use strict";
  return {
    getTimesheetData: function () {
      //return "/rest/gettimereport?begda=2021-04-01&endda=2021-04-30";
      return "/rest/gettimereport";
    },
    getToken: function () {
      return "/rest/getToken";
    },
    getCalendarData: function () {
      return "/rest/getEmpCalendarData";
    },
  };
});