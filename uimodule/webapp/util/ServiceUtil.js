sap.ui.define([
"./ServiceUtil"
], function(){
   "use strict";
   return {
     fnAjaxQuery: function(sUrl,sType,oParams,token) {
       return new Promise(function(resolve,reject) {     
        $.ajax({
          url: sUrl,
          type: sType,
          cache: false,
          contentType: "application/json; charset=utf-8",
           headers: {
            "X-CSRF-Token": token
          },
           data: oParams,
           success: function (result) {
            resolve(result);
          }.bind(this),
          error: function (error) {
            resolve(error);
          }.bind(this)
        });

       }.bind(this));
     },
     fnReadToken: function (sUrl) {
      return new Promise(function (resolve, reject) {
        $.ajax({
          url: sUrl,
          method: "GET",
          async: false,
          headers: {
            "X-CSRF-Token": "Fetch"
          },
          success: function (result, xhr, data) {
            resolve(data.getResponseHeader("X-CSRF-Token"));
          }
        });
      }.bind(this));
    },
     
   };
});