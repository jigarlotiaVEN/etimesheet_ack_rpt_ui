sap.ui.define(["ventia/polaris/etimesheetabs/controller/BaseController"], function (Controller) {
    "use strict";

    return Controller.extend("ventia.polaris.etimesheetabs.controller.App", {
     
      onInit: function () {
        debugger;
      this.oRouter = this.getOwnerComponent().getRouter();
      this.oRouter.attachRouteMatched(this.onRouteMatched, this);
      //this.oRouter.attachBeforeRouteMatched(this.onBeforeRouteMatched, this);
      this.oRouter.navTo("master");
    },

    onRouteMatched: function(oEvent) {

    },

    });
});
