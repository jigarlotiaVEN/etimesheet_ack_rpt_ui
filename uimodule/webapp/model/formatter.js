sap.ui.define([], function () {
  "use strict";
  return {

    dateToYYYYMMDD: function (oDate) {
      var oDateFormat = sap.ui.core.format.DateFormat.getDateInstance({
        pattern: "yyyy-MM-dd"
      });

      var DayFormatted = oDateFormat.format(oDate);
      return DayFormatted;
    },

    dateToDDMMYYYY: function (oDate) {
      //debugger;
      var aDate = oDate.split("-");
      var DayFormatted = aDate[2] + "/" + aDate[1] + "/" + aDate[0];
      return DayFormatted;
    },

    dateToYYYYMMDDSearch: function (oDate) {
      //debugger;
      var aDate = oDate.split("/");
      var DayFormatted = aDate[2] + "-" + aDate[1] + "-" + aDate[0];
      return DayFormatted;
    },

     convertStatusText: function (sStatusText) {
       //debugger;
      if (sStatusText !== undefined) {
        switch (sStatusText) {
          case "O":
            return "None";
            break;
          case "":
            return "None";
            break;
          case "S":
            return "Information";
            break;
          case "A":
            return "Success";
            break;
          case "R":
            return "Error";
            break;
          case "I":
            return "Warning";
            break;
          case "D":
            return "Warning";
            break;
          default:
            return "None";
        }
      }
    },

    convertStatus: function (sStatusText) {
       //debugger;
      if (sStatusText !== undefined) {
        switch (sStatusText) {
          case "O":
            return "None";
            break;
          case "":
            return "None";
            break;
          case "S":
            return "Submitted";
            break;
          case "A":
            return "Success";
            break;
          case "R":
            return "Error";
            break;
          case "I":
            return "Warning";
            break;
          case "D":
            return "Draft";
            break;
          default:
            return "None";
        }
      }
    },
  };
});
