sap.ui.define(["sap/ui/test/Opa5"], function (Opa5) {
    "use strict";

    return Opa5.extend("ventia.polaris.etimesheetabs.test.integration.arrangements.Startup", {
        iStartMyApp: function () {
            this.iStartMyUIComponent({
                componentConfig: {
                    name: "ventia.polaris.etimesheetabs",
                    async: true,
                    manifest: true
                }
            });
        }
    });
});
